from fastapi import APIRouter, Depends, status
from .. import schemas, database, oauth2
from sqlalchemy.orm import Session
from ..repository import user

router = APIRouter(
    tags=["Users"]
)


@router.post("/user", status_code=status.HTTP_201_CREATED)
def create_user(request: schemas.User, db: Session = Depends(database.get_db),
                ):
    return user.create_user(request, db)


@router.get("/user/{id}", response_model=schemas.ShowUser)
def show_user(id: int, db: Session = Depends(database.get_db),
              get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    return user.show_user(id, db)


@router.get("/users")
def show_all_user(db: Session = Depends(database.get_db),
                  ):
    return user.show_all_user(db)
