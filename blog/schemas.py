from pydantic import BaseModel
from typing import List, Optional


class Blog(BaseModel):
    title: str
    body: str

    class Config:
        orm_mode = True


class User(BaseModel):
    name: str
    email: str
    password: str


class ShowUser(BaseModel):
    name: str
    email: str
    blogs: List[Blog] = []

    class Config:
        orm_mode = True


class GetUser(
    BaseModel):  # Creator was connected with show user and was showing blogs also. Hence created GetUser class to show name and email only
    name: str
    email: str

    class Config:
        orm_mode = True


class ShowBlog(BaseModel):
    title: str
    body: str

    creator: GetUser

    class Config:
        orm_mode = True


class Login(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None


class Get_all_blogs(BaseModel):
    id: int
    title: str
    body: str

    class Config:
        orm_mode = True
